
from tp3.dao.db_connection import DBConnection


class InitDb():
    @staticmethod
    def init_db():
        with DBConnection().connection as connection:
            with connection.cursor() as cursor :
                init_db_script = open("./initdb.sql", "r").read()
                cursor.execute(init_db_script)
                return init_db_script