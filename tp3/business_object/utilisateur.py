class Utilisateur:
    __id = None
    def __init__(self,nom:str,prenom:str) -> None:
        self.__nom = nom
        self.__prenom = prenom

    @property
    def nom(self):
        return self.__nom

    @nom.setter
    def nom(self, value):
        self.__nom = value

    @property
    def prenom(self):
        return self.__prenom

    @prenom.setter
    def prenom(self, value):
        self.__prenom = value


    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, value):
        self.__id = value

    def __str__(self):
        id_as_str = ""
        if self.__id is not None:
            id_as_str = "id : "+str(self.__id)+","
        return "{"+id_as_str+"nom : "+self.__nom+", prenom : "+self.__prenom+"}"
    
    def __repr__(self):
        return str(self)