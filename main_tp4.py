
if __name__ == "__main__":
    from tp4_preview.start_view import StartView
    current_view = StartView()
    while current_view:
        # a border between view
        with open('tp4_preview/border.txt', 'r', encoding="utf-8") as asset:
            print(asset.read())
        # Display the info of the view
        current_view.display_info()
        # ask user for a choice
        current_view = current_view.make_choice()
