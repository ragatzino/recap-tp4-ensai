from tp4_preview.singleton import Singleton
from tp3.business_object.utilisateur import Utilisateur

class Session(metaclass=Singleton):
    def __init__(self):
        """
        Définition des variables que l'on stocke en session
        Le syntaxe
        ref:type = valeur
        permet de donner le type des variables. Utile pour l'autocompletion.
        """
        self.user=Utilisateur(nom="doe",prenom="john")
        self.__last_query_resultset = None

    @property
    def last_query_resultset(self):
        return self.__last_query_resultset

    @last_query_resultset.setter
    def last_query_resultset(self, value):
        self.__last_query_resultset = value
