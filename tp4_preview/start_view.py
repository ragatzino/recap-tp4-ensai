from pprint import pprint
from InquirerPy.base.control import Choice
from InquirerPy import inquirer
from tp4_preview.abstract_view import AbstractView
from tp4_preview.session import Session




class StartView(AbstractView):

    def __init__(self):
        self._questions = inquirer.rawlist(
            message=f'Que voulez vous faire ?',
            choices=[
                'Créer la base de données',
                'Requêter le serveur de bdd',
                Choice(value=None, name="Quitter l'application")])
                
        
        

    def display_info(self):
        with open('tp4_preview/banner.txt', 'r', encoding="utf-8") as asset:
            print(asset.read())

    def make_choice(self):
        reponse = self._questions.execute()
        if reponse == 'Nothing':
            pass
        elif reponse== 'Créer la base de données':
            from tp4_preview.create_db import CreateDb
            return CreateDb()
        elif reponse== 'Requêter le serveur de bdd':
            from tp4_preview.requetes.requete_view import RequeteView
            return RequeteView()
