"""
* Checkbox question example
* run example by typing `python example/checkbox.py` in your console
From : https://github.com/CITGuru/PyInquirer/blob/master/examples/checkbox.py
"""
from pprint import pprint

from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from InquirerPy.separator import Separator
from tp3.dao.utilisateur_dao import UtilisateurDao

from tp4_preview.abstract_view import AbstractView
from tp4_preview.requetes.operations.user_view import UserView
from tp4_preview.session import Session
from tp4_preview.start_view import StartView

class RequeteView(AbstractView):
    def __init__(self):
        self._questions = inquirer.select(
            message=f'Que voulez vous faire ?',
            choices=[
                'Definir utilisateur courant',
                'Sauvegarder utilisateur courant',
                'Modifier utilisateur courant',
                'Supprimer utilisateur courant',
                'Retour menu principal'
                ])
                
    def _find_and_print_users(self):
        utilisateur_dao = UtilisateurDao()
        users = utilisateur_dao.find_all_users()
        print(f"users actuels : {users}")    

    def display_info(self):
        print("== Onglet requête ==")
        session = Session()
        print(f"l'utilisateur courant est : {session.user}")
        self._find_and_print_users()

    def make_choice(self):
        reponse = self._questions.execute()
        if reponse == 'Definir utilisateur courant':
            return UserView()
        elif reponse== 'Sauvegarder utilisateur courant':
            from tp4_preview.requetes.operations.create_user_view import CreateUserView
            return CreateUserView()
        elif reponse== 'Modifier utilisateur courant':
            from tp4_preview.requetes.operations.update_current_user import UpdateUserView
            return UpdateUserView()
        elif reponse== 'Supprimer utilisateur courant':
            from tp4_preview.requetes.operations.delete_current_user import DeleteUserView
            return DeleteUserView()
        else :
            return StartView()

