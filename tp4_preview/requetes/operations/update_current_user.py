from tp3.dao.utilisateur_dao import UtilisateurDao

from tp4_preview.abstract_view import AbstractView
from tp4_preview.session import Session

class UpdateUserView(AbstractView):
        
    def display_info(self):
        utilisateur_dao = UtilisateurDao()
        session = Session()
        utilisateur = session.user
        utilisateur_dao.update_user(utilisateur)
        print(f"mise a jour du user {utilisateur}")

    def make_choice(self):
        from tp4_preview.requetes.requete_view import RequeteView
        return RequeteView()

