"""
* Checkbox question example
* run example by typing `python example/checkbox.py` in your console
From : https://github.com/CITGuru/PyInquirer/blob/master/examples/checkbox.py
"""
from ctypes import util
from pprint import pprint

from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from InquirerPy.separator import Separator
from tp3.dao.utilisateur_dao import UtilisateurDao

from tp4_preview.abstract_view import AbstractView
from tp4_preview.session import Session

class UserView(AbstractView):                
        
    def display_info(self):
        session = Session()
        utilisateur = session.user
        print(f"definition de l'utilisateur : {utilisateur}")

    def make_choice(self):
        self.__formulaire()
        from tp4_preview.requetes.requete_view import RequeteView
        return RequeteView()

    def __formulaire(self):
        nom = inquirer.text(
            message="Entrez le nom:",
            default=""
            ).execute()

        prenom = inquirer.text(
            message="Entrez le prenom:",
            default=""
            ).execute()

        session = Session()
        utilisateur = session.user
        if nom is not "" :
            utilisateur.nom = nom
        if prenom is not "":
            utilisateur.prenom = prenom
        session.user = utilisateur