"""
* Checkbox question example
* run example by typing `python example/checkbox.py` in your console
From : https://github.com/CITGuru/PyInquirer/blob/master/examples/checkbox.py
"""
from pprint import pprint

from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from InquirerPy.separator import Separator
from tp3.dao.utilisateur_dao import UtilisateurDao

from tp4_preview.abstract_view import AbstractView
from tp4_preview.session import Session

class DeleteUserView(AbstractView):
        
    def display_info(self):
        utilisateur_dao = UtilisateurDao()
        session = Session()
        utilisateur = session.user
        utilisateur_dao.delete_user(utilisateur)
        print(f"suppression du user {utilisateur}")

    def make_choice(self):
        from tp4_preview.requetes.requete_view import RequeteView
        return RequeteView()

