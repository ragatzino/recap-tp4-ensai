"""
* Checkbox question example
* run example by typing `python example/checkbox.py` in your console
From : https://github.com/CITGuru/PyInquirer/blob/master/examples/checkbox.py
"""
from pprint import pprint

from InquirerPy import inquirer
from InquirerPy.base.control import Choice
from InquirerPy.separator import Separator
from tp3.dao.utilisateur_dao import UtilisateurDao

from tp4_preview.abstract_view import AbstractView
from tp4_preview.start_view import StartView

class CreateDb(AbstractView):
    def __init__(self):
        self._questions = inquirer.rawlist(
            message=f'Que voulez vous faire ?',
            choices=[
                Choice(value=None, name="Retour au menu principal de l'application")])
        
    def display_info(self):
        from tp3.dao.init_db import InitDb
        script = InitDb.init_db()
        print("requête executée : ")
        print(script)
        print("==================")

    def make_choice(self):
        reponse = self._questions.execute()
        if reponse == None:
            return StartView()
        return CreateDb()