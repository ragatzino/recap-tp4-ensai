from abc import ABC, abstractmethod
from InquirerPy import inquirer
from InquirerPy.base.control import Choice
class AbstractView(ABC):

    @abstractmethod
    def display_info(self):
        pass


    @abstractmethod
    def make_choice(self):
        pass
