from tp3.dao.init_db import InitDb
from tp3.dao.utilisateur_dao import UtilisateurDao
from tp3.business_object.utilisateur import Utilisateur
if __name__ == "__main__":
    script = InitDb.init_db()
    print("lancement du script de mise en place de la bdd")
    print(script)
    print("=====")
    utilisateur = Utilisateur(nom="Doe",prenom="John")
    utilisateur_dao = UtilisateurDao()
    def _find_and_print_users():
        users = utilisateur_dao.find_all_users()
        print(f"users actuels : {users}")
    _find_and_print_users()
    utilisateur_dao.save_user(utilisateur)
    print(f"ajout du user {utilisateur}")
    _find_and_print_users()
    utilisateur = utilisateur_dao.find_all_users()[0]
    utilisateur.nom = "Johnny"
    utilisateur_dao.update_user(utilisateur)
    print("renommage du user en Johnny")
    _find_and_print_users()
    utilisateur_dao.delete_user(utilisateur)
    print("delete du user")
    _find_and_print_users()