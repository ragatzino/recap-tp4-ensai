DROP TABLE IF EXISTS utilisateur;

CREATE TABLE IF NOT EXISTS utilisateur 
(
    id serial,
    nom VARCHAR(100),
    prenom VARCHAR(100)
);